package org.basket.common;

import com.codeborne.selenide.ex.TimeoutException;
import org.testng.Assert;

import java.util.Random;
import static com.codeborne.selenide.Configuration.pageLoadTimeout;
import static com.codeborne.selenide.Selenide.Wait;
import static com.codeborne.selenide.WebDriverRunner.url;



public class Utils {

    public static final String PASSWORD = "atdtest";

    public static String mailinatorMailRandom() {
        Random randomGenerator = new Random();
        int random = randomGenerator.nextInt();
        return "autotest" + random + "@mailinator.com";
    }

    public static void checkingContainsUrl(String expectedContainsUrl) {
        pageLoadTimeout = 200000;
        try {
            Wait().until(webDriver -> url().contains(expectedContainsUrl));
        } catch (TimeoutException e) {
            System.out.println(url());
            Assert.fail("Url doesn't contains: " + expectedContainsUrl);
        }
    }
}
