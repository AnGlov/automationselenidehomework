package org.basket.common;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;

public interface Cookies {

    /**
     * Локатор для работы с куки прпапом
     */

    default SelenideElement closeCookiesBtn() {
        return $x("(//a[contains(@class,'popup-choose-cookies__btn--blue')])[1]");
    }


    default void closeCookiesPopupBtn() {
        try {
            closeCookiesBtn().shouldBe(Condition.appear, Duration.ofSeconds(20));
            closeCookiesBtn().click();
            closeCookiesBtn().shouldNotBe(Condition.visible, Duration.ofSeconds(5));
            System.out.println("Cookies block closed");
        } catch (UIAssertionError e) {
            System.out.println("Cookies block doesn't appears");
        }



    }

}
