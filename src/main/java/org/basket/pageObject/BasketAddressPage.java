package org.basket.pageObject;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;


import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.*;
import static org.basket.common.Utils.checkingContainsUrl;

public class BasketAddressPage {

    /**
     * Локатор для меню переходов корзины
     * */

    private SelenideElement cartStepLink(int stepNumber) {
        return $x("//li[@data-step='" + stepNumber + "']");

    }

    /**
     * Локатор для поля Фамилия
     * */

    private SelenideElement lastName() {
        return $x("//input[@name='lVorname']");
    }

    /**
     * Локатор для поля Имени
     * */

    private SelenideElement firstName() {
        return $x("//input[@name='lName']");
    }

    /**
     * Локатор для поля Улицы
     * */

    private SelenideElement street() {
        return $x("//input[@name='lStrasse']");
    }

    /**
     * Локатор для поля Номера дома
     * */
    private SelenideElement deliveryHouse() {
        return $x("//input[@name='delivery_house']");
    }

    /**
     * Локатор для поля Индекса
     * */
    private SelenideElement zipCode() {
        return $x("//input[@name='lPlz']");
    }

    /**
     * Локатор для поля Города
     * */

    private SelenideElement city() {
        return $x("//input[@name='lOrt']");
    }

    /**
     * Локатор для поля Страны
     * */

    private SelenideElement deliveryCountry(String country) {
        return $x("//*[@name='lLand']//*[@data-code='" + country + "']");
    }
    /**
     * Локатор для поля Номера телефона
     * */

    public SelenideElement telephoneNumber() {
        return $x("//input[@name='lTelefon']");
    }

    /**
     * Локатор для кнопки Следующий шаг
     * */

    private SelenideElement nextStepBtn() {
        return $x("//div[@class='button-continue address-continue']/a");
    }

    /**
     * Локатор для кнопки "На главную""
     * */
    private SelenideElement logo() {
        return $x("//div[@class='cart-page-head__logo']//img");
    }

    /**
     * Локатор для Shipping form (проверить, что залогинились и перешли на страницу адреса)
     * */
    private SelenideElement shippingForm() {
        return $x("//div[@class='shipping-form input-form']");
    }

//----------------------------------------------------------------------------------------------------------------------


    public BasketAddressPage checkShippingForm() {
        shippingForm().shouldBe(visible, ofSeconds(10));
        return this;
    }
    public BasketAddressPage checkCorrectTextAndFillInput(SelenideElement element, String correctText) {
        Configuration.fastSetValue = false;
        if (!element.getValue().equals(correctText)) {
            element.clear();
            element.setValue(correctText);
        }
        return this;
    }

    public BasketAddressPage fillAllFieldsForShip(String lastName, String firstName, String street, String deliveryHouse,
                                                  String zipCode, String city, String country, String telephoneNumber) {
        checkCorrectTextAndFillInput(lastName(), lastName);
        checkCorrectTextAndFillInput(firstName(), firstName);
        checkCorrectTextAndFillInput(street(), street);
        checkCorrectTextAndFillInput(deliveryHouse(), deliveryHouse);
        checkCorrectTextAndFillInput(zipCode(), zipCode);
        checkCorrectTextAndFillInput(city(), city);
        chooseDeliveryCountryForShipping(country.toUpperCase());
        checkCorrectTextAndFillInput(telephoneNumber(), telephoneNumber);
        return this;
    }

    public BasketAddressPage chooseDeliveryCountryForShipping(String country) {
        deliveryCountry(country).shouldBe(visible).click();
        return this;
    }

    public BasketPaymentsPage clickNextStep() {
        nextStepBtn().click();
        return page(BasketPaymentsPage.class);
    }

    public MainPage clickOnLogo() {
        logo().shouldBe(visible, ofSeconds(5)).click();
        return page(MainPage.class);
    }

    public void clickOnCartStepLink(int stepNumber, String expectedURL) {
        cartStepLink(stepNumber).click();
        checkingContainsUrl(expectedURL);
    }


}
