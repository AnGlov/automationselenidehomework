package org.basket.pageObject;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import org.basket.common.Cookies;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;
import static java.time.Duration.ofSeconds;

public class ProductPage implements Cookies {

    /**
     * Локатор превью корзині в хедере
     */
    private SelenideElement headerCart() {
        return $x("//div[@class='header-cart__count']");
    }



    /**
    * Локатор для кнопки "Купить"
    */

    private SelenideElement buyBtn() {
        return $x("//div[@class='product-button button ']");
    }

    /**
     * Локатор для кнопки закрытия попапа альтернатив
     */

    private SelenideElement closePopupRelatedBtn() {
        return $x("//div[contains(@class,'popup-related-goods__close')]");
    }

    private SelenideElement cartPreview() {
        return $x("//a[@class='header-cart__link']");
    }

    private SelenideElement proceedToCheckoutBtn() {
        return $x("//a[@class='btn ga-click temp-ga-click']");
    }

    /**
     * Локатор для значення номер артикула
     */

    private SelenideElement productId() {
        return $x("//div[@class='product-button button ']");
    }


    //------------------------------------------------------------------------------------------------------------------


    /**
     * Методы
     */

    public ProductPage clickOnBuyBtn() {
        buyBtn().click();
        return this;
    }

    public ProductPage closeRelatedPopupBtn() {
        try {
            closePopupRelatedBtn().shouldBe(appear, Duration.ofSeconds(20));
            closePopupRelatedBtn().click();
            closePopupRelatedBtn().shouldNotBe(visible, Duration.ofSeconds(5));
            System.out.println("Related popup closed");
        } catch (UIAssertionError e) {
            System.out.println("Related popup doesn't appears");
        }
        return this;
    }

    public BasketAccountPage clickOnProceedToCheckoutBtn() {
        cartPreview().shouldBe(visible, ofSeconds(5)).hover();
        proceedToCheckoutBtn().click();
        return page(BasketAccountPage.class);
    }

    public BasketPage clickOnHeaderCart() {
        headerCart().scrollIntoView("{block:\"center\"}").click();
        return page(BasketPage.class);
    }

    public String getAttributeId() {
        return productId().getAttribute("id");
    }
}