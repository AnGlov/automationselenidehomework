package org.basket.pageObject;


import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import org.testng.Assert;


import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.*;

public class SuccessPage {

    /**
     * Локаторы для поп-апа сбора ВИН номеров
     */

    private SelenideElement vinPopUp() {
        return $x("//div[@class='popup-check-vin']");
    }

    private SelenideElement vinInput(String productId) {
        return $x("//div[@class='vin-number']//input[@data-article-id='" + productId + "']");
    }

    private SelenideElement closePopUpAfterOrderBtn() {
        return $x("//span[@class='popup-after-order__close']");
    }

    private SelenideElement newOrderNumberBlock() {
        return $x("//div[@class='success-order__info']");
    }

    private ElementsCollection tipsForVin(int inputIndex) {
        return $$x("//div[contains(@class,'vin-number')]//input[@data-article-id='" + inputIndex + "']");
    }

    private SelenideElement tipsDropdown(int inputIndex) {
        return $x("(//div[@class='vin-number__dropdown'])[" + inputIndex + "]");
    }

    //------------------------------------------------------------------------------------------------------------------

    public SuccessPage closePopUpAfterOrder() {
        try {
            closePopUpAfterOrderBtn().shouldBe(appear, ofSeconds(20));
            closePopUpAfterOrderBtn().click();
            closePopUpAfterOrderBtn().shouldNotBe(visible, ofSeconds(5));
            System.out.println("Popup after order closed");
        } catch (UIAssertionError e) {
            System.out.println("Popup after order doesn't appears");
        }
        return this;
    }

    public SuccessPage checkingOfOrderCreating() {
        newOrderNumberBlock().shouldBe(visible, ofSeconds(10));
        return this;
    }

    public SuccessPage checkVinPopUp() {
        vinPopUp().shouldBe(visible, ofSeconds(5));
        return this;
    }

    public SuccessPage enterVinNumber(String productId, String VinNum) {
        vinInput(productId).scrollIntoView("{block:\"center\"}").setValue(VinNum);
        return this;
    }

    public SuccessPage clearVinNumber(String productId) {
        vinInput(productId).scrollIntoView("{block:\"center\"}").clear();
        return this;
    }

    public SuccessPage checkTipsForVin(int inputIndex, String... expectedTipsForVin) {
        tipsForVin(inputIndex).get(0).scrollIntoView("{block:\"center\"}").shouldBe(visible);
        ArrayList<String> actualTipsForVin = new ArrayList<>();
        for (int i = 0; i < tipsForVin(inputIndex).size(); i++) {
            actualTipsForVin.add(tipsForVin(inputIndex).get(i).getText());
        }
        Assert.assertEquals(Arrays.asList(expectedTipsForVin), actualTipsForVin, "Tips for VINs is not expected");
        return this;
    }

    public SuccessPage checkAbsenceTipsDropdown(int inputIndex) {
        tipsDropdown(inputIndex).shouldNotBe(visible);
        return this;
    }
}


