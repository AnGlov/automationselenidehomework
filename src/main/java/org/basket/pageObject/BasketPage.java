package org.basket.pageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class BasketPage {
    /**
     * Локатор для блока СО-инфо
     */

    private SelenideElement soInfoBlock() {
        return $x("//div[contains(@class,'bestelen-block__col--text')]");
    }
    /**
     * Локатор для кнопки следующий шаг
     */

    private SelenideElement nextStepBtn() {
        return $x("//a[@data-ga-action='Next_click']");
    }

    /**
     * Локатор для вызова тултипа Оней
     */

    private SelenideElement oneyTooltipBtn() {
        return $x("//div[@class='oney-text oney-tooltip-btn']/p");
    }

    private SelenideElement oneyTooltipImages() {
        return $x("//div[@class='oney-tooltip__images']");
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Metods
     * */
    public BasketPage checkNumberOfDaysInSOBlockForDeAndFrShops(String shop, String daysNumber ) {
        if (shop.equalsIgnoreCase("DE") || shop.equalsIgnoreCase("FR")) {
            String actualDaysNumber = soInfoBlock().getText().replaceAll("\\D", "");
            Assert.assertTrue(actualDaysNumber.equals(daysNumber), "Expected amount of days does not coincides with actual!");
        } else {
            System.out.println("Shop isn't equals to DE and FR");
        }
        return this;
    }

    public BasketAccountPage clickToNextStep() {
        nextStepBtn().click();
        return page(BasketAccountPage.class);
    }

        public BasketPage checkOneyTooltipPresence() {
            oneyTooltipBtn().scrollIntoView("{block:\"center\"}").click();
            oneyTooltipImages().shouldBe(Condition.visible);
            return this;
        }

}
