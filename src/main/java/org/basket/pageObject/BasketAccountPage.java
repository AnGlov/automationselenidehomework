package org.basket.pageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class BasketAccountPage {

    /**
     * Локатор для поля Email
     * */
    private SelenideElement emailInput() {
        return $x("//input[@name='Email']");
    }

    /**
     * Локатор для поля Email
     * */
    private SelenideElement passwordInput() {
        return $x("//input[@name='Password']");
    }

    /**
     * Локатор для поля Submit
     * */
    private SelenideElement submitBtn() {
        return $x("//a[@class='login']");
    }

    /**
     * Локатор для полей регистрации
     * */

    private SelenideElement emailInputForRegistration() {
        return $x("(//input[@id='form_Email'])[2]");
    }

    private SelenideElement passwordInputForRegistration() {
        return $x("(//input[@id='form_Password'])[2]");
    }

    private SelenideElement submitBtnForRegistration() {
        return $x("(//div[@class='button-continue']/a)[2]");
    }

    private SelenideElement authorisationForm() {
        return $x("//div[@class='signin-user-box-form']");
    }


//---------------------------------------------------------------------------------------------------------------------

/**
 * Methods
 * */
    public BasketAddressPage logIn(String mail, String password) {
        emailInput().shouldBe(visible, Duration.ofSeconds(10)).setValue(mail);
        emailInput().shouldHave(value(mail));
        passwordInput().setValue(password);
        passwordInput().shouldHave(value(password));
        submitBtn().click();
        return page(BasketAddressPage.class);
    }

    public BasketAddressPage registration(String mail, String password) {
        emailInputForRegistration().setValue(mail);
        emailInputForRegistration().shouldHave(value(mail));
        passwordInputForRegistration().setValue(password);
        passwordInputForRegistration().shouldHave(value(password));
        submitBtnForRegistration().click();
        return page(BasketAddressPage.class);
    }

    public BasketAccountPage checkingPresenceOfAuthorisationForm() {
        authorisationForm().shouldBe(Condition.visible, Duration.ofSeconds(5));
        return this;
    }

}
