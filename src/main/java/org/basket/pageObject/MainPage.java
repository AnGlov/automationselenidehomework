package org.basket.pageObject;


import com.codeborne.selenide.SelenideElement;
import org.basket.common.Cookies;


import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.*;

public class MainPage implements Cookies {

    /**
     * Локатор для кнопки разлогина
     * */

    private SelenideElement logoutBtn() {
        return $(byCssSelector(".logout_but"));
    }


    /**
     * Локатор для кнопки логина
     * */

    private SelenideElement loginBtn() {
        return $(byCssSelector(".header-i--user"));
    }


    /**
     * Локаторы для логин поп-апа
     * */

    private SelenideElement loginPopUp() {
        return $(byCssSelector(".popup_login"));
    }

    private SelenideElement emailInput() {
        return $x("//input[@name='Email']");
    }

    private SelenideElement passwordInput() {
        return $x("//input[@name='Password']");
    }

    private SelenideElement submitBtn() {
        return $x("//a[@class='enter submit']");
    }



    //------------------------------------------------------------------------------------------------------------------
    /**
     * Методы
     * */

    public MainPage logout() {
        closeCookiesBtn();
        logoutBtn().shouldBe(visible, ofSeconds(10)).click();
        logoutBtn().shouldNotBe(visible, ofSeconds(5));
        return this;
    }


    public ProfilePage loginFromHeader(String mail, String password) {
        loginBtn().shouldBe(visible, ofSeconds(5)).click();
        loginPopUp().shouldBe(visible, ofSeconds(5));
        emailInput().setValue(mail);
        emailInput().shouldHave(value(mail));
        passwordInput().setValue(password);
        passwordInput().shouldHave(value(password));
        submitBtn().click();
        return page(ProfilePage.class);
    }
}
