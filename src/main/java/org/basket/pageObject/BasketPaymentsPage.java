package org.basket.pageObject;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class BasketPaymentsPage {

    /**
     * Локатор для метода оплаты Банк
     *
     * */

    private SelenideElement bank(){
        return $x("//input[@id='hypovereinsbank']");

    }

    /**
     * Локатор для кнопки Следующи шаг
     *
     * */

    private SelenideElement nextStepBtn() {
        return $x("//div[@class='button-continue']/a");
    }
//----------------------------------------------------------------------------------------------------------------------
    /**
     * Методы
     *
     * */

    public BasketPaymentsPage selectBankPaymentMethod() {
        bank().click();
        return this;
    }

    public AllDataPage clickToNextStep() {
        nextStepBtn().click();
        return page(AllDataPage.class);

    }
}
