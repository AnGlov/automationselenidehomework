package org.basket.pageObject;


import com.codeborne.selenide.SelenideElement;



import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.*;

public class ProfilePage {

    /**
     * Локатор для поля Имени
     * */


    private SelenideElement topTitle() {
        return $x("//div[@class='top_title']");
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Методы
     * */

    public ProfilePage checkingTopTitle() {
        topTitle().shouldBe(visible, ofSeconds(5));
        return this;
    }



}
