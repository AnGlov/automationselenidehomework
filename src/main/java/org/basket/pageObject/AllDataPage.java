package org.basket.pageObject;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class AllDataPage {

    /**
     * Локатор для блока СО-инфо
     */

    private SelenideElement soInfoBlock() {
        return $x("//div[contains(@class,'bestelen-block__col--text')]");
    }

    /**
     * Локатор для кнопки Купить
     */

    private SelenideElement buyBtnAlldata() {
        return $x("//a[@class='next-step']");
    }

    private SelenideElement grandTotalBlock() {
        return $x("//div[@class='order-summary order-summary--top']");

    }


//----------------------------------------------------------------------------------------------------------------------
    /**
     * Методы
     */
    public AllDataPage checkingPresenceOfGrandTotalBlock() {
        grandTotalBlock().shouldBe(Condition.visible, Duration.ofSeconds(5));
        return this;
    }
    public AllDataPage checkNumberOfDaysInSOBlockForDeAndFrShops(String shop, String daysNumber ) {
        if (shop.equalsIgnoreCase("DE") || shop.equalsIgnoreCase("FR")) {
            String actualDaysNumber = soInfoBlock().getText().replaceAll("\\D", "");
            Assert.assertTrue(actualDaysNumber.equals(daysNumber), "Expected amount of days does not coincides with actual!");
        } else {
            System.out.println("Shop isn't equals to DE and FR");
        }
        return this;
    }

    public SuccessPage clickOnBuyBtn() {
        buyBtnAlldata().click();
        return page(SuccessPage.class);
    }
}
