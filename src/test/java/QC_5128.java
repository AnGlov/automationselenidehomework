import org.basket.pageObject.MainPage;
import org.basket.pageObject.ProductPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;


public class QC_5128 extends BaseTest {

    private ProductPage productPage = new ProductPage();
    private MainPage mainPage = new MainPage();
    private String mail = "ana.glovatskaya@gmail.com";
    private String password = "Ti2zevek";

    @Test
    public void workOfStandardRedirectsInBasketAfterLogout() {
        open("https://www.autodoc.de/neolux/11767542");
        productPage.closeCookiesPopupBtn();
        productPage.clickOnBuyBtn()
                .closeRelatedPopupBtn()
                .clickOnHeaderCart()
                .clickToNextStep()
                .logIn(mail, password)
                .fillAllFieldsForShip("AUTODOC", "SE", "Joseff-Orlopp-Strase",
                        "55", "10365", "Berlin", "DE", "200+002")
                .clickNextStep()
                .selectBankPaymentMethod()
                .clickToNextStep()
                .clickOnBuyBtn()
                .closePopUpAfterOrder()
                .checkingOfOrderCreating();
        mainPage.logout();
        open("https://www.autodoc.de/neolux/11767542");
        productPage.clickOnBuyBtn()
                .clickOnProceedToCheckoutBtn()
                .checkingPresenceOfAuthorisationForm();
    }
}
